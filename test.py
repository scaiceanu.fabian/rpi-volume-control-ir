import os
from flask import Flask, render_template, request
app = Flask(__name__)

@app.route("/")
def index():
        return render_template('index.html')

@app.route("/<action>")
def action(action):
        if action == "on":
            os.system('irsend SEND_ONCE monoceros VOL_UP')
        if action == "off":
            os.system('irsend SEND_ONCE monoceros VOL_DN')
        return render_template('index.html')
if __name__ == "__main__":
        app.run(host='0.0.0.0', port=81, debug=True)